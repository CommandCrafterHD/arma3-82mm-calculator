# Arma3 82mm calculator
A simple calculator for the Mk6 82mm mortar. I would suggest running the script through a console like git bash because the normal
console doesn't display colors but instead just a weird symbol... if you don't have the know-how, don't worry! You can still just run
the .bat script!

## How do I run the script?
Well thats easy! If you have git installed on your PC, just right click into the folder and click on "git bash". In that console you
type "python main.py". If you don't have git installed just double click the file called run.bat (this will look weird because windows
apparently just won't do colors in the console).
There's also a new .bat script called "browser.bat" if you want to use this, first run "requirements.bat" to install the "flask" module.
After that, just run the "browser.bat"! (It won't look GOOD yet, because I just started adding the webserver, but its a bit better than
the console)

## How do I use the script after I've run it?
To use the script just input the values the calculator asks for, which are:

- The elevation of the mortar (in meters)
- The elevation of the target (in meters)
- The distance between the mortar and the target (in meters)
- The angle from the mortar to the target (in mils)
- The crosswind (in meters per second)
- The headwind (in meters per second)
- The wind direction (left or right)*
- The wind direction (front or back)*

(*In the web version of the calculator the wind direction is just one selection)

## How do I get these values?
**To get the elevation** just locate yourself and your target on the map and look for the closest elevation information.

**To get the direction and distance** locate yourself and your target and then place your map tools on top of those and
point the map tools from the mortar to the target, the direction is the outer number (on the white surface) and the 
distance can be found out by moving the map tools over so the edge of the tool lines up with both points like a ruler.

**To get the wind direction and speed** you press SHIFT+K to see the direction, then turn so the arrow points at you.
Now you take out your Kestrel and go to the "crosswind" screen. Now press the middle button until it says "direction set".
Finally you turn towards your target (again, check your map tools for the direction) and look at your Kestrel, it should
now tell you the crosswind in the middle and right below that the headwind. And using SHIFT+K you can directly get the wind
direction!

## What does the calculator telle me?
The calculator will tell you these values:
- Charge (0, 1 or 2. Press F in the mortar to change charges)
- Elevation
- Mils (These 2 values are how you have to aim the mortar)
- Flighttime (in case you need to know WHEN the shell hits)
- Mpm correction*
- Epm correction*

*the Mpm/Epm correction is in case you need to manually correct the target.
the Mpm is mils per meter, so if you need to move it right by five meters you have to calculate (5 * Mpm)

the Epm is elevation per meter, so if you need to move it back by eight meters you have to calculate (8 * Epm)

Made by: Bates aka. CCHD