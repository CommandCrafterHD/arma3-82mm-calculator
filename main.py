from files import colors
from webserver.static.calculator import calculate


def getInput(title, lowerLimit, upperLimit):
    inputGot = input(f"{colors.GREEN}{title}{colors.END}: ").replace(',', '.')
    try:
        if lowerLimit <= float(inputGot) <= upperLimit:
            return float(inputGot)
        else:
            print(f"{colors.RED}ERROR: That value does not work here{colors.END}")
            return getInput(title, lowerLimit, upperLimit)
    except:
        print(f"{colors.RED}ERROR: The value should be a NUMBER{colors.END}")
        return getInput(title, lowerLimit, upperLimit)


def showResult(finals):
    longestkey = 0
    longestvalue = 0

    for k in finals.keys():
        if len(k) > longestkey:
            longestkey = len(k)
        if len(str(finals[k])) > longestvalue:
            longestvalue = len(str(finals[k]))
        if isinstance(finals[k], float):
            finals[k] = "{:.2f}".format(finals[k])

    print()
    print("Done! (Mpm is Mils per meter)")
    print()
    print("=" * (10 + (longestvalue + longestkey)))
    for k in finals.keys():
        print(f"||{colors.MAGENTA} {k.title()}{' ' * (longestkey - len(k))} {colors.END}|| {colors.GREEN}{finals[k]}{' ' * (longestvalue - len(str(finals[k])))} {colors.END}||")
        print("=" * (10+(longestvalue+longestkey)))
    print()
    print("Preparing for new input...")
    print()
    print("." * 50)


def main():
    values = {}
    print()
    values['mortar_height'] = getInput("Mortar Height (m)", 0, 100000)
    values['target_height'] = getInput("Target Height (m)", 0, 100000)
    values['distance'] = getInput("Distance (m)", 50, 3100)
    values['angle'] = getInput("Angle (mil)", 0, 6400)
    values['crosswind'] = getInput("Crosswind (m/s)", 0, 100)
    values['headwind'] = getInput("Headwind (m/s)", 0, 100)
    values['wind_lr'] = getInput("Wind from left (0) or right (1)", 0, 1)
    values['wind_fb'] = getInput("Wind from front (0) or back (1)", 0, 1)
    print("." * 50)
    results = calculate(values)
    showResult(results)
    main()


if __name__ == "__main__":
    print("." * 50, colors.BLUE)
    print(",-----.            ,--.               ,--.")
    print("|  |) /_  ,--,--.,-'  '-. ,---.  ,---.|  |")
    print("|  .-.  \\' ,-.  |'-.  .-'| .-. :(  .-'`-'")
    print("|  '--' /\ '-'  |  |  |  \   --..-'  `) ")
    print("`------'  `--`--'  `--'   `----'`----' ")
    print(f"{colors.END}mortar calculator")
    print("." * 50)
    main()

