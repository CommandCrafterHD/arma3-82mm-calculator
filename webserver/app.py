from flask import Flask, render_template, request
from static.calculator import calculate
import webbrowser

app = Flask(__name__)


def test():
    print("So does it work like this?")


@app.route('/', methods=['POST', 'GET'])
def calculator():
    data = request.form.get('morelev', 0)
    if data is 0:
        pass
    else:
        values = {}
        values['mortar_height'] = request.form.get('morelev')
        values['target_height'] = request.form.get('tarelev')
        values['distance'] = request.form.get('distance')
        values['angle'] = request.form.get('direction')
        values['crosswind'] = request.form.get('cwspeed')
        values['headwind'] = request.form.get('hwspeed')
        if request.form.get('wdirection') == str(0):
            values['wind_lr'] = 0
            values['wind_fb'] = 1
        elif request.form.get('wdirection') == str(1):
            values['wind_lr'] = 0
            values['wind_fb'] = 0
        elif request.form.get('wdirection') == str(2):
            values['wind_lr'] = 1
            values['wind_fb'] = 0
        elif request.form.get('wdirection') == str(3):
            values['wind_lr'] = 1
            values['wind_fb'] = 1

        finals = calculate(values)

        for k in finals.keys():
            if isinstance(finals[k], float):
                finals[k] = "{:.2f}".format(finals[k])

        return render_template('calculator.html', values=finals)
    finals = {'elevation': '---', 'mils': '---', 'flighttime': '---', 'mpm correction': '---', 'epm': '---'}
    return render_template('calculator.html', values=finals)


if __name__ == "__main__":
    webbrowser.open('http://127.0.0.1:5000')
    app.run()
