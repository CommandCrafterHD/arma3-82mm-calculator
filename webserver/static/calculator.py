import json

try:
    rangetables = json.load(open('files/rangetables.json'))
except:
    rangetables = json.load(open('../files/rangetables.json'))

tableindex = {
    "ch0": range(50, 350, 50),
    "ch1": range(350, 1250, 50),
    "ch2": range(1250, 3100, 50)
}


def roundto50(x, base=50):
    return base * round(float(x)/base)


def getEpM(elv1, elv2):
    if elv1 > elv2:
        return (elv1-elv2)/50
    else:
        return (elv2 - elv1) / 50


def calculate(values):
    finals = {}

    for k in values.keys():
        if values[k]:
            values[k] = float(values[k])

    # Get charge and EpM

    if values['distance'] < 350:
        finals['charge'] = 0
        values['rangetable'] = rangetables['charge_0'][tableindex["ch0"].index(roundto50(values['distance']))]
        if values['rangetable']['range'] == 50:
            values['epm'] = 1.08
        else:
            if values['rangetable']['range'] < values['distance']:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_0'][tableindex["ch0"].index(roundto50(values['distance']) + 50)]['elev'])
            else:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_0'][tableindex["ch0"].index(roundto50(values['distance']) - 50)]['elev'])
    elif values['distance'] < 1250:
        finals['charge'] = 1
        values['rangetable'] = rangetables['charge_1'][tableindex["ch1"].index(roundto50(values['distance']))]
        if values['rangetable']['range'] == 350:
            values['epm'] = 0.3
        else:
            if values['rangetable']['range'] < values['distance']:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_1'][tableindex["ch1"].index(roundto50(values['distance']) + 50)]['elev'])
            else:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_1'][tableindex["ch1"].index(roundto50(values['distance']) - 50)]['elev'])
    else:
        finals['charge'] = 2
        values['rangetable'] = rangetables['charge_2'][tableindex["ch2"].index(roundto50(values['distance']))]
        if values['rangetable']['range'] == 1250:
            values['epm'] = 0.18
        else:
            if values['rangetable']['range'] < values['distance']:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_2'][tableindex["ch2"].index(roundto50(values['distance']) + 50)]['elev'])
            else:
                values['epm'] = getEpM(values['rangetable']['elev'], rangetables['charge_2'][tableindex["ch2"].index(roundto50(values['distance']) - 50)]['elev'])

    finals['elevation'] = values['rangetable']['elev']
    finals['mils'] = values['angle']

    # Accounting for rounding to the nearest value
    if values['distance'] > values['rangetable']['range']:
        finals['elevation'] -= ((values['distance'] - values['rangetable']['range']) * values['epm'])
    else:
        finals['elevation'] += ((values['rangetable']['range'] - values['distance']) * values['epm'])

    # Accounting for height differences
    if values['mortar_height'] > values['target_height']:
        finals['elevation'] += ((values['mortar_height'] - values['target_height']) / 100) * values['rangetable']['elevp100']
    else:
        finals['elevation'] -= ((values['target_height'] - values['mortar_height']) / 100) * values['rangetable']['elevp100']

    # Horizontal correction
    if values['wind_lr'] == 0:
        finals['mils'] -= (values['crosswind'] * values['rangetable']['milcorr'])
    else:
        finals['mils'] += (values['crosswind'] * values['rangetable']['milcorr'])

    # Vertical correction
    if values['wind_fb'] == 0:
        finals['elevation'] -= ((values['headwind'] * values['rangetable']['headwcorr']) * values['epm'])
    else:
        finals['elevation'] += ((values['headwind'] * values['rangetable']['tailwcorr']) * values['epm'])

    # Calculating time
    finals['flighttime'] = values['rangetable']['tof']
    if values['mortar_height'] > values['target_height']:
        finals['flighttime'] += (values['rangetable']['tofp100'] * ((values['mortar_height'] - values['target_height']) / 100))
    else:
        finals['flighttime'] += (values['rangetable']['tofp100'] * ((values['target_height'] - values['mortar_height']) / 100))

    # List some more values
    finals['mpm correction'] = values['distance']/1000
    finals['epm'] = values['epm']

    return finals